import mongoose, { Schema } from "mongoose";

const userSchema = new Schema({
    role: { type: String, maxlength: 30, required: true },
    name: { type: String, maxlength: 50, unique: true, required: true },
    type_document: { type: String, maxlength: 20 },
    number_document: { type: String, maxlength: 20 },
    adress: { type: String, maxlength: 70 },
    phone: { type: String, maxlength: 20 },
    email: { type: String, maxlength: 50, unique: true, required: true },
    password: { type: String, maxlength: 70, required: true },
    status: { type: Number, default: 1 },
    createdAt: { type: Date, default: Date.now }
});


const User = mongoose.model('user', userSchema);
export default User;
