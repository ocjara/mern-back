import routerx from "express-promise-router";
import cartegoriaRouter from "./category";
import articleRouter from "./article";
import userRouter from "./user";

const router = routerx();

router.use("/category", cartegoriaRouter);
router.use("/article", articleRouter);
router.use("/user", userRouter);

export default router;
