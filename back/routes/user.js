import routerx from "express-promise-router";
import UserController from "../controllers/UserController";

const router = routerx();

router.post("/add", UserController.add);
router.get("/query", UserController.query);
router.get("/list", UserController.list);
router.put("/update", UserController.update);
router.delete("/remove", UserController.remove);
router.put("/activate", UserController.activate);
router.put("/deactivate", UserController.deactivate);
router.post("/login", UserController.login);
export default router;