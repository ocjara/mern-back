import models from "../models";
import bcrypt from "bcryptjs";
import { model } from "mongoose";
import token from "../services/token";

export default {
    add: async (req, res, next) => {
        try {
            req.body.password = await bcrypt.hash(req.body.password, 10);
            const reg = await models.User.create(req.body);
            res.status(200).json(reg);
        } catch (e) {
            res.status(500).send({
                message: "Ocurrio un problema",
            });
            next(e);
        }
    },
    query: async (req, res, next) => {
        try {
            const reg = await models.User.findOne({ _id: req.query._id });
            if (!reg) {
                res.status(404).send({
                    message: "No existe el registro",
                });
            } else {
                res.status(200).json(reg);
            }
        } catch (e) {
            res.status(500).send({
                message: "Ocurrio un problema",
            });
            next(e);
        }
    },
    list: async (req, res, next) => {
        try {
            let valor = req.query.valor;
            const reg = await models.User.find(
                {
                    $or: [
                        { name: new RegExp(valor, "i") },
                        { email: new RegExp(valor, "i") },
                    ],
                },
                { createAt: 0 }
            ).sort({ createAt: -1 });
            res.status(200).json(reg);
        } catch (e) {
            res.status(500).send({
                message: "Ocurrio un problema",
            });
            next(e);
        }
    },
    update: async (req, res, next) => {
        try {
            let pass = req.body.password
            const reg0 = await model.User.findOne({ _id: req.body._id })
            if (pass != reg0.password) {
                req.body.password = await bcrypt.hash(req.body.password, 10);
            }
            const reg = await models.User.findByIdAndUpdate(
                { _id: req.body._id },
                { name: req.body.name, role: req.body.role, type_document: req.body.type_document, adress: req.body.adress, phone: req.body.phone, password: req.body.password }
            );
            res.status(200).json(reg);
        } catch (e) {
            res.status(500).send({
                message: "Ocurrio un problema",
            });
            next(e);
        }
    },
    remove: async (req, res, next) => {
        try {
            const reg = await models.User.findByIdAndDelete({
                _id: req.body._id,
            });
            res.status(200).json(reg);
        } catch (e) {
            res.status(500).send({
                message: "Ocurrio un problema",
            });
            next(e);
        }
    },
    activate: async (req, res, next) => {
        try {
            const reg = await models.User.findByIdAndUpdate(
                { _id: req.body._id },
                { status: 1 }
            );
            res.status(200).json(reg);
        } catch (e) {
            res.status(500).send({
                message: "Ocurrio un problema",
            });
            next(e);
        }
    },
    deactivate: async (req, res, next) => {
        try {
            const reg = await models.User.findByIdAndUpdate(
                { _id: req.body._id },
                { status: 0 }
            );
            res.status(200).json(reg);
        } catch (e) {
            res.status(500).send({
                message: "Ocurrio un problema",
            });
            next(e);
        }
    },
    login: async (req, res, next) => {
        try {
            let user = await models.User.findOne({ email: req.body.email, status: 1 });
            if (user) {
                let match = await bcrypt.compare(req.body.password, user.password);
                if (match) {
                    let tokenReturn = await token.encode(user._id);
                    res.status(200).json({ user, tokenReturn });
                } else {
                    res.status(404).send({
                        message: "password incorrecto"
                    });
                }
            } else {
                res.status(404).send({
                    message: "No existe el usuario"
                });
            }
        } catch (e) {
            res.status(500).send({
                message: "Ocurrio un problema",
            });
            next(e);
        }
    }
};
